package com.seeyou.api.money.model;

import com.seeyou.common.enums.MoneyType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MoneyHistoryRequest {
    @ApiModelProperty(notes = "년도", required = true)
    @NotNull
    private String moneyYear;

    @ApiModelProperty(notes = "월", required = true)
    @NotNull
    private String moneyMonth;

    @ApiModelProperty(notes = "급여타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MoneyType moneyType;

    @ApiModelProperty(notes = "세전금액", required = true)
    @NotNull
    @Min(value = 0)
    private Double beforeMoney;

    @ApiModelProperty(notes = "국민연금", required = true)
    @NotNull
    @Min(value = 0)
    private Double nationalPension;

    @ApiModelProperty(notes = "건강보험", required = true)
    @NotNull
    @Min(value = 0)
    private Double healthInsurance;

    @ApiModelProperty(notes = "장기요양보험", required = true)
    @NotNull
    @Min(value = 0)
    private Double longTermCareInsurance;

    @ApiModelProperty(notes = "고용보험", required = true)
    @NotNull
    @Min(value = 0)
    private Double employmentInsurance;

    @ApiModelProperty(notes = "산재보험", required = true)
    @NotNull
    @Min(value = 0)
    private Double industrialAccidentInsurance;

    @ApiModelProperty(notes = "소득세", required = true)
    @NotNull
    @Min(value = 0)
    private Double incomeTax;

    @ApiModelProperty(notes = "지방소득세", required = true)
    @NotNull
    @Min(value = 0)
    private Double localIncomeTax;
}