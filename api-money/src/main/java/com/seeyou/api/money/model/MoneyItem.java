package com.seeyou.api.money.model;

import com.seeyou.api.money.entity.Money;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MoneyItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "직원명")
    private String member;

    @ApiModelProperty(notes = "급여타입")
    private String moneyType;

    @ApiModelProperty(notes = "세전금액")
    private BigDecimal beforeMoney;

    private MoneyItem(Builder builder) {
        this.id = builder.id;
        this.member = builder.member;
        this.moneyType = builder.moneyType;
        this.beforeMoney = builder.beforeMoney;
    }

    public static class Builder implements CommonModelBuilder<MoneyItem> {
        private final Long id;
        private final String member;
        private final String moneyType;
        private final BigDecimal beforeMoney;

        public Builder(Money money) {
            this.id = money.getId();
            this.member = money.getMember().getMemberName();
            this.moneyType = money.getMoneyType().getName();
            this.beforeMoney = CommonFormat.convertDoubleToDecimal(money.getBeforeMoney());
        }

        @Override
        public MoneyItem build() {
            return new MoneyItem(this);
        }
    }
}
