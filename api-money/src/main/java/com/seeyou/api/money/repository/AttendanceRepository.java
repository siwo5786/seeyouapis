package com.seeyou.api.money.repository;

import com.seeyou.api.money.entity.Attendance;
import com.seeyou.api.money.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    List<Attendance> findAllByAttendanceYearAndAttendanceMonthAndMember(int year, int month, Member member);
}