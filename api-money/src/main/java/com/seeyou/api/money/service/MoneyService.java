package com.seeyou.api.money.service;

import com.seeyou.api.money.entity.Member;
import com.seeyou.api.money.entity.Money;
import com.seeyou.api.money.model.MoneyItem;
import com.seeyou.api.money.model.MoneyRequest;
import com.seeyou.api.money.model.MoneyResponse;
import com.seeyou.api.money.model.MoneySearchRequest;
import com.seeyou.api.money.repository.MoneyRepository;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MoneyService {
    private final MoneyRepository moneyRepository;
    private final EntityManager entityManager;

    public void setMoney(Member member, MoneyRequest moneyRequest) {
        Optional<Money> money = moneyRepository.findByMember(member);
        if (money.isPresent()) throw new CMissingDataException();
        Money addData = new Money.Builder(member, moneyRequest).build();
        moneyRepository.save(addData);
    }

    public ListResult<MoneyItem> getMoneyList(int page, MoneySearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Money> originList = getMoneyList(pageRequest, request);

        List<MoneyItem> result = new LinkedList<>();


        originList.forEach(e -> result.add(new MoneyItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Money> getMoneyList(Pageable pageable, MoneySearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Money> criteriaQuery = criteriaBuilder.createQuery(Money.class);

        Root<Money> root = criteriaQuery.from(Money.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getName() != null) predicates.add(criteriaBuilder.like(root.get("member").get("memberName"), "%" + request.getName() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Money> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public MoneyResponse getMoneyLists(long id) {
        Money money = moneyRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MoneyResponse.Builder(money).build();
    }

    public void putMoney(long id, MoneyRequest moneyRequest) {
        Money money = moneyRepository.findById(id).orElseThrow(CMissingDataException::new);
        money.putMoney(moneyRequest);
        moneyRepository.save(money);
    }
}