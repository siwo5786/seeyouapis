package com.seeyou.api.money.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneySearchRequest {
    private String name;
}
