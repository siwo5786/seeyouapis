package com.seeyou.api.money.repository;

import com.seeyou.api.money.entity.Member;
import com.seeyou.api.money.entity.MoneyHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MoneyHistoryRepository extends JpaRepository<MoneyHistory, Long> {
    List<MoneyHistory> findByMember(Member member);
    Optional<MoneyHistory> findByIdAndMember(long id, Member member);
    Optional<MoneyHistory> findByMemberAndMoneyYearAndMoneyMonth(Member member, String year, String month);
}
