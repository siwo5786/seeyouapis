package com.seeyou.api.board.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BoardRequest {
    @ApiModelProperty(notes = "제목(2~30)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String title;

    @ApiModelProperty(notes = "내용(10~)", required = true)
    @NotNull
    @Length(min = 10)
    private String content;
}
