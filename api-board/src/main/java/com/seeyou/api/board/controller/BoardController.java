package com.seeyou.api.board.controller;

import com.seeyou.api.board.model.BoardItem;
import com.seeyou.api.board.model.BoardRequest;
import com.seeyou.api.board.model.BoardResponse;
import com.seeyou.api.board.model.BoardSearchRequest;
import com.seeyou.api.board.service.BoardService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "게시글 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;

    @ApiOperation(value = "게시글 등록")
    @PostMapping("/data")
    public CommonResult setBoard(@RequestBody @Valid BoardRequest request) {
        boardService.setBoard(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 리스트")
    @GetMapping("/all/{page}")
    public ListResult<BoardItem> getList(
            @PathVariable int page,
            @RequestParam(value = "searchTitle", required = false) String searchTitle
            ) {
        BoardSearchRequest request = new BoardSearchRequest();
        request.setTitle(searchTitle);
        return ResponseService.getListResult(boardService.getList(page, request), true);
    }

    @ApiOperation(value = "게시글 리스트 상세")
    @GetMapping("/board")
    public SingleResult<BoardResponse> getBoard(@RequestParam("id") int id) {
        return ResponseService.getSingleResult(boardService.getBoard(id));
    }


    @ApiOperation(value = "게시글 수정")
    @PutMapping("/{id}")
    public CommonResult putBoard(@PathVariable long id, @RequestBody @Valid BoardRequest request) {
        boardService.putBoard(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delBoard(@PathVariable long id) {
        boardService.delBoard(id);
        return ResponseService.getSuccessResult();
    }
}