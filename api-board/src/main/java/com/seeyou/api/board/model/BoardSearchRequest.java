package com.seeyou.api.board.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BoardSearchRequest {
    private String title;
}
