package com.seeyou.api.board.service;

import com.seeyou.api.board.entity.Board;
import com.seeyou.api.board.model.BoardItem;
import com.seeyou.api.board.model.BoardRequest;
import com.seeyou.api.board.model.BoardResponse;
import com.seeyou.api.board.model.BoardSearchRequest;
import com.seeyou.api.board.repository.BoardRepository;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {
    private final BoardRepository boardRepository;
    private final EntityManager entityManager;

    public void setBoard(BoardRequest request) {
        Board board = new Board.Builder(request).build();
        boardRepository.save(board);
    }

    public ListResult<BoardItem> getList(int page, BoardSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Board> originList = getList(pageRequest, request);

        List<BoardItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new BoardItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Board> getList(Pageable pageable, BoardSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Board> criteriaQuery = criteriaBuilder.createQuery(Board.class);

        Root<Board> root = criteriaQuery.from(Board.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getTitle() != null) predicates.add(criteriaBuilder.like(root.get("title"), "%" + request.getTitle() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Board> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public BoardResponse getBoard(long id) {
        Board board = boardRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new BoardResponse.Builder(board).build();
    }

    public void putBoard(long id, BoardRequest request) {
        Board board = boardRepository.findById(id).orElseThrow();
        board.putBoard(request);
        boardRepository.save(board);
    }

    public void delBoard(long id) {
        boardRepository.deleteById(id);
    }
}
