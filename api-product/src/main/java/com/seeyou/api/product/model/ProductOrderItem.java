package com.seeyou.api.product.model;

import com.seeyou.api.product.entity.ProductOrder;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrderItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "단가")
    private BigDecimal unitPrice;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    @ApiModelProperty(notes = "발주일")
    private String dateOrder;

    @ApiModelProperty(notes = "완료여부")
    private String isComplete;

    private ProductOrderItem(Builder builder) {
        this.id = builder.id;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.unitPrice = builder.unitPrice;
        this.price = builder.price;
        this.dateOrder = builder.dateOrder;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<ProductOrderItem> {
        private final Long id;
        private final String productName;
        private final Integer quantity;
        private final BigDecimal unitPrice;
        private final BigDecimal price;
        private final String dateOrder;
        private final String isComplete;


        public Builder(ProductOrder productOrder) {
            this.id = productOrder.getId();
            this.productName = productOrder.getProduct().getProductName();
            this.quantity = productOrder.getQuantity();
            this.unitPrice = CommonFormat.convertDoubleToDecimal(productOrder.getProduct().getUnitPrice());
            this.price = CommonFormat.convertDoubleToDecimal(productOrder.getPrice());
            this.dateOrder = productOrder.getOrderYear() + "-" + productOrder.getOrderMonth() + "-" + productOrder.getOrderDay() + " " +  productOrder.getTimeOrder().getHour() + ":" + productOrder.getTimeOrder().getMinute();
            this.isComplete = productOrder.getIsComplete() ? "O" : "X";
        }

        @Override
        public ProductOrderItem build() {
            return new ProductOrderItem(this);
        }
    }
}
