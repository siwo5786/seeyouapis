package com.seeyou.api.product.service;

import com.seeyou.api.product.entity.ProductOrder;
import com.seeyou.api.product.model.ProductOrderCompleteItem;
import com.seeyou.api.product.model.ProductOrderItem;
import com.seeyou.api.product.model.ProductOrderResponse;
import com.seeyou.api.product.model.ProductOrderSearchRequest;
import com.seeyou.api.product.repository.ProductOrderRepository;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductOrderService {
    private final ProductOrderRepository productOrderRepository;
    private final EntityManager entityManager;

    public ProductOrder getProductData(long id) {
        return productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public ListResult<ProductOrderItem> getList(int page, ProductOrderSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<ProductOrder> originList = getList(pageRequest, request);

        List<ProductOrderItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new ProductOrderItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<ProductOrder> getList(Pageable pageable, ProductOrderSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<ProductOrder> criteriaQuery = criteriaBuilder.createQuery(ProductOrder.class);

        Root<ProductOrder> root = criteriaQuery.from(ProductOrder.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getOrderYear() != null) predicates.add(criteriaBuilder.equal(root.get("orderYear"), request.getOrderYear()));
        if (request.getOrderMonth() != null) predicates.add(criteriaBuilder.equal(root.get("orderMonth"), request.getOrderMonth()));
        if (request.getOrderDay() != null) predicates.add(criteriaBuilder.equal(root.get("orderDay"), request.getOrderDay()));
        if (request.getIsComplete() != null) predicates.add(criteriaBuilder.equal(root.get("isComplete"), request.getIsComplete()));


        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<ProductOrder> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public ProductOrderResponse getProductOrder(long id) {
        ProductOrder productOrder = productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ProductOrderResponse.Builder(productOrder).build();
    }

    // 발주 상태 수정
    public void putProductOrder(long id) {
        ProductOrder productOrder = productOrderRepository.findById(id).orElseThrow(CMissingDataException::new);
        productOrder.putProductOrder();
        productOrderRepository.save(productOrder);
    }
}