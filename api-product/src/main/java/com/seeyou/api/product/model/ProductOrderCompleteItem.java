package com.seeyou.api.product.model;

import com.seeyou.api.product.entity.ProductOrder;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductOrderCompleteItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long productOrderId;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "단가")
    private BigDecimal unitPrice;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    @ApiModelProperty(notes = "발주일")
    private String dateOrder;

    @ApiModelProperty(notes = "완료일")
    private LocalDateTime completeDateOrder;

    @ApiModelProperty(notes = "완료여부")
    private String isComplete;

    private ProductOrderCompleteItem(Builder builder) {
        this.productOrderId = builder.productOrderId;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.unitPrice = builder.unitPrice;
        this.price = builder.price;
        this.dateOrder = builder.dateOrder;
        this.completeDateOrder = builder.completeDateOrder;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<ProductOrderCompleteItem> {
        private final Long productOrderId;
        private final String productName;
        private final Integer quantity;
        private final BigDecimal unitPrice;
        private final BigDecimal price;
        private final String dateOrder;
        private final LocalDateTime completeDateOrder;
        private final String isComplete;

        public Builder(ProductOrder productOrder) {
            this.productOrderId = productOrder.getId();
            this.productName = productOrder.getProduct().getProductName();
            this.quantity = productOrder.getQuantity();
            this.unitPrice = CommonFormat.convertDoubleToDecimal(productOrder.getProduct().getUnitPrice());
            this.price = CommonFormat.convertDoubleToDecimal(productOrder.getPrice());
            this.dateOrder = productOrder.getOrderYear() + "-" + productOrder.getOrderMonth() + "-" + productOrder.getOrderDay() + " " +  productOrder.getTimeOrder().getHour() + ":" + productOrder.getTimeOrder().getMinute();
            this.completeDateOrder = productOrder.getCompleteDateOrder();
            this.isComplete = productOrder.getIsComplete() ? "O" : "X";
        }

        @Override
        public ProductOrderCompleteItem build() {
            return new ProductOrderCompleteItem(this);
        }
    }
}