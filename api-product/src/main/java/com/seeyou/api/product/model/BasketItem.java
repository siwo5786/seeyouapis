package com.seeyou.api.product.model;

import com.seeyou.api.product.entity.Basket;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BasketItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품명")
    private String product;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "가격")
    private BigDecimal price;

    private BasketItem(Builder builder) {
        this.id = builder.id;
        this.product = builder.product;
        this.quantity = builder.quantity;
        this.price = builder.price;
    }
    public static class Builder implements CommonModelBuilder<BasketItem> {
        private final Long id;
        private final String product;
        private final Integer quantity;
        private final BigDecimal price;

        public Builder(Basket basket) {
            this.id = basket.getId();
            this.product = basket.getProduct().getProductName();
            this.quantity = basket.getQuantity();
            this.price = CommonFormat.convertDoubleToDecimal(basket.getPrice());
        }

        @Override
        public BasketItem build() {
            return new BasketItem(this);
        }
    }
}
