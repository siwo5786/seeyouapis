package com.seeyou.api.product.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductOrderSearchRequest {
    private String orderYear;
    private String orderMonth;
    private String orderDay;
    private Boolean isComplete;
}
