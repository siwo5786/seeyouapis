package com.seeyou.api.product.controller;


import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.model.StockItem;
import com.seeyou.api.product.model.StockResponse;
import com.seeyou.api.product.model.StockRequest;
import com.seeyou.api.product.model.StockSearchRequest;
import com.seeyou.api.product.service.ProductService;
import com.seeyou.api.product.service.StockService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "재고 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/stock")
public class StockController {
    private final StockService stockService;

    @ApiOperation(value = "재고 리스트")
    @GetMapping("/all/{page}")
    public ListResult<StockItem> getStockList(
            @PathVariable int page,
            @RequestParam(value = "searchName", required = false) String searchName
            ) {
        StockSearchRequest request = new StockSearchRequest();
        request.setName(searchName);
        return ResponseService.getListResult(stockService.getStockList(page, request), true);
    }

    @ApiOperation(value = "재고 리스트 상세")
    @GetMapping("/stock")
    public SingleResult<StockResponse> getStock(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(stockService.getStock(id));
    }

    @ApiOperation(value = "재고 수정")
    @PutMapping("/update/{id}")
    public CommonResult putStock(@PathVariable long id, @RequestBody @Valid StockRequest request) {
        stockService.putStock(id, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "재고 부족 리스트")
    @GetMapping("/lack-list")
    public ListResult<StockItem> getStockLackList() {
       return ResponseService.getListResult(stockService.getStockLackList(), true);
    }
}