package com.seeyou.api.product.service;

import com.seeyou.api.product.entity.Basket;
import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.entity.ProductOrder;
import com.seeyou.api.product.model.BasketItem;
import com.seeyou.api.product.model.BasketRequest;
import com.seeyou.api.product.model.ProductOrderRequest;
import com.seeyou.api.product.repository.BasketRepository;
import com.seeyou.api.product.repository.ProductOrderRepository;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BasketService {
    private final BasketRepository basketRepository;
    private final ProductOrderRepository productOrderRepository;

    public void setBasket(Product product, BasketRequest request) {
        Optional<Basket> basket = basketRepository.findByProduct(product);
        if (basket.isPresent()) throw new CMissingDataException();

        Basket addData = new Basket.Builder(product, request).build();
        basketRepository.save(addData);
    }

    public ListResult<BasketItem> getList() {
        List<Basket> originList = basketRepository.findAll();

        List<BasketItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new BasketItem.Builder(e).build()));

        return ListConvertService.settingResult(result);
    }

    public void putBasket(long id, BasketRequest request) {
        Basket basket = basketRepository.findById(id).orElseThrow(CMissingDataException::new);
        basket.putBasket(request);
        basketRepository.save(basket);
    }

    public void delBasket(long id) {
        basketRepository.deleteById(id);
    }

    public void delBasket() {
        List<Basket> originList = basketRepository.findAll();

        for (Basket basket : originList) {
            ProductOrderRequest request = new ProductOrderRequest();
            request.setQuantity(basket.getQuantity());
            ProductOrder productOrder = new ProductOrder.Builder(basket.getProduct(), request).build();
            productOrderRepository.save(productOrder);
        }

        basketRepository.deleteAll();
    }
}