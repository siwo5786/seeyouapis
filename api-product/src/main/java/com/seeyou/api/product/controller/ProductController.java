package com.seeyou.api.product.controller;

import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.model.*;
import com.seeyou.api.product.service.ProductService;
import com.seeyou.api.product.service.StockService;
import com.seeyou.common.enums.ProductType;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(tags = "상품 관리")
@RequiredArgsConstructor
@RequestMapping(value = "/v1/product")
public class ProductController {
    private final ProductService productService;
    private final StockService stockService;

    @PostMapping("/new")
    @ApiOperation(value = "상품 등록")
    public CommonResult setProduct(@RequestBody @Valid ProductRequest productRequest) {
        Product product = productService.setProduct(productRequest);
        stockService.setStock(product);
        return ResponseService.getSuccessResult();
    }
    @GetMapping("/all/{page}")
    @ApiOperation(value = "상품 리스트")
    public ListResult<ProductItem> getProductList(
            @PathVariable int page,
            @RequestParam(value = "searchType", required = false) ProductType searchType,
            @RequestParam(value = "searchName", required = false) String searchName
            ) {
        ProductSearchRequest request = new ProductSearchRequest();
        request.setProductType(searchType);
        request.setProductName(searchName);

        return ResponseService.getListResult(productService.getProductList(page, request), true);
    }

    @GetMapping("/product")
    @ApiOperation(value = "상품 리스트 상세")
    public SingleResult<ProductResponse> getProduct(@RequestParam("id") long id) {
        return ResponseService.getSingleResult(productService.getProduct(id));
    }

    @PutMapping("/update/{id}")
    @ApiOperation(value = "상품 수정")
    public CommonResult putProduct(@PathVariable long id, @RequestBody @Valid ProductUpdateRequest request) {
        productService.putProduct(id, request);
        return ResponseService.getSuccessResult();
    }
}