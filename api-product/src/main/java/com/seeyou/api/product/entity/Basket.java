package com.seeyou.api.product.entity;

import com.seeyou.api.product.model.BasketRequest;
import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private Double price;

    public void putBasket(BasketRequest request) {
        this.quantity = request.getQuantity();
        this.price = product.getUnitPrice() * request.getQuantity();
    }

    private Basket(Builder builder) {
        this.product = builder.product;
        this.quantity = builder.quantity;
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<Basket> {
        private final Product product;
        private final Integer quantity;
        private final Double price;

        public Builder(Product product, BasketRequest request) {
            this.product = product;
            this.quantity = request.getQuantity();
            this.price = product.getUnitPrice() * request.getQuantity();
        }

        @Override
        public Basket build() {
            return new Basket(this);
        }
    }
}