package com.seeyou.api.product.controller;

import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.model.BasketItem;
import com.seeyou.api.product.model.BasketRequest;
import com.seeyou.api.product.service.BasketService;
import com.seeyou.api.product.service.ProductService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "장바구니 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/basket")
public class BasketController {
    private final BasketService basketService;
    private final ProductService productService;

    @ApiOperation(value = "장바구니 등록")
    @PostMapping("/product-id/{productId}")
    public CommonResult setBasket(@PathVariable long productId, @RequestBody @Valid BasketRequest request) {
        Product product = productService.getProductData(productId);
        basketService.setBasket(product, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "장바구니 리스트")
    @GetMapping("/product-id/list")
    public ListResult<BasketItem> getList(){
        return ResponseService.getListResult(basketService.getList(), true);
    }

    @ApiOperation(value = "장바구니 수정")
    @PutMapping("/{id}")
    public CommonResult putBasket(@PathVariable long id, @RequestBody @Valid BasketRequest request) {
        basketService.putBasket(id, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "장바구니 삭제")
    @DeleteMapping("{id}")
    public CommonResult delBasket(@PathVariable long id) {
        basketService.delBasket(id);
        return ResponseService.getSuccessResult();
    }
}