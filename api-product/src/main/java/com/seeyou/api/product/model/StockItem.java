package com.seeyou.api.product.model;

import com.seeyou.api.product.entity.Stock;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class StockItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "재고수량")
    private Integer stockQuantity;

    @ApiModelProperty(notes = "최소기준수량")
    private Integer minQuantity;

    private StockItem(Builder builder) {
        this.id = builder.id;
        this.productName = builder.productName;
        this.stockQuantity = builder.stockQuantity;
        this.minQuantity = builder.minQuantity;
    }

    public static class Builder implements CommonModelBuilder<StockItem> {
        private final Long id;
        private final String productName;
        private final Integer stockQuantity;
        private final Integer minQuantity;

        public Builder(Stock stock) {
            this.id = stock.getId();
            this.productName = stock.getProduct().getProductName();
            this.stockQuantity = stock.getStockQuantity();
            this.minQuantity = stock.getMinQuantity();
        }

        @Override
        public StockItem build() {
            return new StockItem(this);
        }
    }
}
