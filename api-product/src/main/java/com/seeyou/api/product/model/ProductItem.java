package com.seeyou.api.product.model;

import com.seeyou.api.product.entity.Product;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ProductItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품타입")
    private String productType;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "단가")
    private BigDecimal unitPrice;

    private ProductItem(Builder builder) {
        this.id = builder.id;
        this.productType = builder.productType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
    }

    public static class Builder implements CommonModelBuilder<ProductItem> {
        private final Long id;
        private final String productType;
        private final String productName;
        private final BigDecimal unitPrice;

        public Builder(Product product) {
            this.id = product.getId();
            this.productType = product.getProductType().getName();
            this.productName = product.getProductName();
            this.unitPrice = CommonFormat.convertDoubleToDecimal(product.getUnitPrice());
        }

        @Override
        public ProductItem build() {
            return new ProductItem(this);
        }
    }
}
