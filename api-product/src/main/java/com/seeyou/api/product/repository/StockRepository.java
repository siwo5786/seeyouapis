package com.seeyou.api.product.repository;

import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.entity.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StockRepository extends JpaRepository<Stock, Long> {
    Optional<Stock> findByProduct (Product product);
}