package com.seeyou.api.product.service;

import com.seeyou.api.product.entity.Product;
import com.seeyou.api.product.model.*;
import com.seeyou.api.product.repository.ProductRepository;
import com.seeyou.common.exception.CAccessDeniedException;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final ProductRepository productRepository;
    private final EntityManager entityManager;

    public Product getProductData(long id) {
        return productRepository.findById(id).orElseThrow(CAccessDeniedException::new);
    }

    public Product setProduct(ProductRequest productRequest) {
        Product product = new Product.ProductBuilder(productRequest).build();
        return productRepository.save(product);
    }

    public ListResult<ProductItem> getProductList(int page, ProductSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Product> originList = getProductList(pageRequest, request);

        List<ProductItem> result = new LinkedList<>();

        originList.forEach(e -> result.add(new ProductItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Product> getProductList(Pageable pageable, ProductSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);

        Root<Product> root = criteriaQuery.from(Product.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getProductType() != null) predicates.add(criteriaBuilder.equal(root.get("productType"), request.getProductType()));
        if (request.getProductName() != null) predicates.add(criteriaBuilder.like(root.get("productName"), "%" + request.getProductName() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Product> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public ProductResponse getProduct(long id) {
        Product product = productRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new ProductResponse.Builder(product).build();
    }

    public void putProduct(long id, ProductUpdateRequest request) {
        Product product = productRepository.findById(id).orElseThrow(CMissingDataException::new);
        product.putProduct(request);
        productRepository.save(product);
    }
}


