package com.seeyou.api.product.model;

import com.seeyou.common.enums.ProductType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSearchRequest {
    private ProductType productType;
    private String productName;
}
