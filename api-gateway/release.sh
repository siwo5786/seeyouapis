#!/bin/bash

# chmod 755 release.sh

# variables
gcpArea="asia.gcr.io"
projectId="original-future-398100"
gcpRegion="asia-northeast3"
gkeClusterName="seeyou-cluster-1"
dockerImage="api-gateway"

# script
docker login -u _json_key --password-stdin https://asia.gcr.io < key.json
docker build -t ${dockerImage} .
docker build -t api-gateway .
docker tag ${dockerImage} "${gcpArea}/${projectId}/${dockerImage}"
docker tag api-gateway "asia.gcr.io/original-future-398100/api-gateway"
docker push "${gcpArea}/${projectId}/${dockerImage}:latest"
docker push "asia.gcr.io/original-future-398100/api-gateway:latest"

#gcloud auth activate-service-account --key-file key.json
export USE_GKE_GCLOUD_AUTH_PLUGIN=True
gcloud container clusters get-credentials ${gkeClusterName} --region ${gcpRegion} --project ${projectId}
kubectl delete deployment ${dockerImage}
kubectl apply -f k8s-deployment.yaml
kubectl apply -f k8s-service.yaml
kubectl apply -f k8s-ingress.yaml

gcloud container clusters get-credentials seeyou-cluster-1 --region asia-northeast3 --project original-future-398100