package com.seeyou.common.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
