package com.seeyou.api.member.controller;

import com.seeyou.common.exception.CAccessDeniedException;
import com.seeyou.common.exception.CAuthenticationEntryPointException;
import com.seeyou.common.response.model.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException();
    }
}
