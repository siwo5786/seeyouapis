package com.seeyou.api.member.model;

import com.seeyou.common.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class MemberSearchRequest {
    private String memberName;

    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    private Boolean isEnabled;
}
