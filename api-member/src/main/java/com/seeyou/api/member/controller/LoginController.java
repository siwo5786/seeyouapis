package com.seeyou.api.member.controller;

import com.seeyou.api.member.model.LoginRequest;
import com.seeyou.api.member.model.LoginResponse;
import com.seeyou.api.member.service.LoginService;
import com.seeyou.common.enums.MemberGroup;
import com.seeyou.common.response.model.SingleResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "웹 - 점장 로그인")
    @PostMapping("/web/owner")
    public SingleResult<LoginResponse> doLoginOwner(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_OWNER, loginRequest, "WEB"));
    }

    @ApiOperation(value = "앱 - 매니저 로그인")
    @PostMapping("/app/manager")
    public SingleResult<LoginResponse> doLoginManager(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_MANAGER, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 사원 로그인")
    @PostMapping("/app/employee")
    public SingleResult<LoginResponse> doLoginEmployee(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_EMPLOYEE, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 알바 로그인")
    @PostMapping("/app/part-time")
    public SingleResult<LoginResponse> doLoginPartTime(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_PART_TIME, loginRequest, "APP"));
    }
}