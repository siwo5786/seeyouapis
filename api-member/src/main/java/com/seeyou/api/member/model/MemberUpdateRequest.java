package com.seeyou.api.member.model;

import com.seeyou.common.enums.MemberGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberUpdateRequest {
    @ApiModelProperty(notes = "직원명(2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    @ApiModelProperty(notes = "직급", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MemberGroup memberGroup;

    @ApiModelProperty(notes = "연락처(13)", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String contact;
}
