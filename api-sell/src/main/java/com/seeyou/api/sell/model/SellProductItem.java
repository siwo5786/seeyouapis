package com.seeyou.api.sell.model;

import com.seeyou.api.sell.entity.SellProduct;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellProductItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "상품타입")
    private String sellProductType;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "단가")
    private BigDecimal unitPrice;

    @ApiModelProperty(notes = "상태")
    private String isEnabled;

    private SellProductItem(Builder builder) {
        this.id = builder.id;
        this.sellProductType = builder.sellProductType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<SellProductItem> {
        private final Long id;
        private final String sellProductType;
        private final String productName;
        private final BigDecimal unitPrice;
        private final String isEnabled;

        public Builder(SellProduct sellProduct) {
            this.id = sellProduct.getId();
            this.sellProductType = sellProduct.getSellProductType().getName();
            this.productName = sellProduct.getProductName();
            this.unitPrice = CommonFormat.convertDoubleToDecimal(sellProduct.getUnitPrice());
            this.isEnabled = sellProduct.getIsEnabled() ? "O" : "X";
        }

        @Override
        public SellProductItem build() {
            return new SellProductItem(this);
        }
    }
}