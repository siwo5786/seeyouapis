package com.seeyou.api.sell.model;

import com.seeyou.api.sell.entity.SellProduct;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class SellProductResponse {
    @ApiModelProperty(notes = "상품타입")
    private String sellProductType;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "단가")
    private BigDecimal unitPrice;

    @ApiModelProperty(notes = "상태")
    private String isEnabled;

    private SellProductResponse(Builder builder) {
        this.sellProductType = builder.sellProductType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<SellProductResponse> {
        private final String sellProductType;
        private final String productName;
        private final BigDecimal unitPrice;
        private final String isEnabled;

        public Builder(SellProduct sellProduct) {
            this.sellProductType = sellProduct.getSellProductType().getName();
            this.productName = sellProduct.getProductName();
            this.unitPrice = CommonFormat.convertDoubleToDecimal(sellProduct.getUnitPrice());
            this.isEnabled = sellProduct.getIsEnabled() ? "O" : "X";
        }

        @Override
        public SellProductResponse build() {
            return new SellProductResponse(this);
        }
    }
}

