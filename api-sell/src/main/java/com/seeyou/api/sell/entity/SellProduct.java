package com.seeyou.api.sell.entity;

import com.seeyou.api.sell.model.SellProductRequest;
import com.seeyou.api.sell.model.SellProductUpdateRequest;
import com.seeyou.common.enums.SellProductType;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private SellProductType sellProductType;

    @Column(nullable = false, length = 20)
    private String productName;

    @Column(nullable = false)
    private Double unitPrice;

    @Column(nullable = false)
    private Boolean isEnabled;

    public void putSellProduct(SellProductUpdateRequest request) {
        this.sellProductType = request.getSellProductType();
        this.productName = request.getProductName();
        this.isEnabled = request.getIsEnabled();
        this.unitPrice = request.getUnitPrice();

    }

    private SellProduct(Builder builder) {
        this.sellProductType = builder.sellProductType;
        this.productName = builder.productName;
        this.unitPrice = builder.unitPrice;
        this.isEnabled = builder.isEnabled;
    }

    public static class Builder implements CommonModelBuilder<SellProduct> {

        private final SellProductType sellProductType;
        private final String productName;
        private final Double unitPrice;
        private final Boolean isEnabled;

        public Builder(SellProductRequest sellProductRequest) {
            this.sellProductType = sellProductRequest.getSellProductType();
            this.productName = sellProductRequest.getProductName();
            this.unitPrice = sellProductRequest.getUnitPrice();
            this.isEnabled = true;
        }

        @Override
        public SellProduct build() {
            return new SellProduct(this);
        }
    }

}



