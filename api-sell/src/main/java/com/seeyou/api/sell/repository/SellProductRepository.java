package com.seeyou.api.sell.repository;

import com.seeyou.api.sell.entity.SellProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SellProductRepository extends JpaRepository<SellProduct, Long> {
}
