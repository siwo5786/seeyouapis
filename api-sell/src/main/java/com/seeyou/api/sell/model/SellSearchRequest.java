package com.seeyou.api.sell.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellSearchRequest {
    private String sellYear;
    private String sellMonth;
    private String sellDay;
}