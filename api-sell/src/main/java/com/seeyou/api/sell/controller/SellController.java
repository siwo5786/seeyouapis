package com.seeyou.api.sell.controller;

import com.seeyou.api.sell.model.SellItem;
import com.seeyou.api.sell.model.SellSearchRequest;
import com.seeyou.api.sell.service.SellService;
import com.seeyou.common.response.model.CommonResult;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "판매 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/sell")
public class SellController {
    private final SellService sellService;

    @ApiOperation(value = "판매 등록")
    @PostMapping("/file-upload")
    public CommonResult setSells(@RequestParam("csvFile") MultipartFile csvFile) throws Exception {
        sellService.setSells(csvFile);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "판매 리스트")
    @GetMapping("/all/{page}")
    public ListResult<SellItem> getSellList(
            @PathVariable int page,
            @RequestParam(value = "searchYear", required = false) String searchYear,
            @RequestParam(value = "searchMonth", required = false) String searchMonth,
            @RequestParam(value = "searchDay", required = false) String searchDay
            ) {
        SellSearchRequest request = new SellSearchRequest();
        request.setSellYear(searchYear);
        request.setSellMonth(searchMonth);
        request.setSellDay(searchDay);
        return ResponseService.getListResult(sellService.getList(page, request), true);
    }

    @ApiOperation(value = "영수증 상세 정보")
    @GetMapping("/billNumber-detail")
    public ListResult<SellItem> getBillNumberDetailList(@RequestParam(value = "billNumber") long billNumber) {
        return ResponseService.getListResult(sellService.getBillNumberDetailList(billNumber), true);
    }

    @ApiOperation(value = "상품 환불")
    @PutMapping("/refund")
    public CommonResult putIsCompleteUpdate(@RequestParam(value = "billNumber") long billNumber) {
        sellService.putIsCompleteUpdate(billNumber);
        return ResponseService.getSuccessResult();
    }
}