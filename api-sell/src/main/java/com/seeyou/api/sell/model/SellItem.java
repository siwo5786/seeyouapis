package com.seeyou.api.sell.model;

import com.seeyou.api.sell.entity.Sell;
import com.seeyou.common.function.CommonFormat;
import com.seeyou.common.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SellItem {
    @ApiModelProperty(notes = "시퀀스")
    private Long id;

    @ApiModelProperty(notes = "영수증번호")
    private Long billNumber;

    @ApiModelProperty(notes = "상품명")
    private String productName;

    @ApiModelProperty(notes = "수량")
    private Integer quantity;

    @ApiModelProperty(notes = "금액")
    private BigDecimal price;

    @ApiModelProperty(notes = "날짜")
    private String dateSell;

    @ApiModelProperty(notes = "환불일")
    private String dateRefund;

    @ApiModelProperty(notes = "완료여부")
    private String isComplete;

    private SellItem(Builder builder) {
        this.id = builder.id;
        this.billNumber = builder.billNumber;
        this.productName = builder.productName;
        this.quantity = builder.quantity;
        this.price = builder.price;
        this.dateSell = builder.dateSell;
        this.dateRefund = builder.dateRefund;
        this.isComplete = builder.isComplete;
    }

    public static class Builder implements CommonModelBuilder<SellItem> {
        private final Long id;
        private final Long billNumber;
        private final String productName;
        private final Integer quantity;
        private final BigDecimal price;
        private final String dateSell;
        private final String dateRefund;
        private final String isComplete;

        public Builder(Sell sell) {
            this.id = sell.getId();
            this.billNumber = sell.getBillNumber();
            this.productName = sell.getSellProduct().getProductName();
            this.quantity = sell.getQuantity();
            this.price = CommonFormat.convertDoubleToDecimal(sell.getPrice());
            this.dateSell = sell.getSellYear() + "-" + sell.getSellMonth() + "-" + sell.getSellDay() + " " + sell.getTimeSell().getHour() + ":" + (sell.getTimeSell().getMinute() < 10 ? "0" + sell.getTimeSell().getMinute() : sell.getTimeSell().getMinute());
            this.dateRefund = sell.getDateRefund() == null ? "-" : CommonFormat.convertLocalDateTimeToString(sell.getDateRefund());
            this.isComplete = sell.getIsComplete() ? "O" : "X";
        }

        @Override
        public SellItem build() {
            return new SellItem(this);
        }
    }
}
