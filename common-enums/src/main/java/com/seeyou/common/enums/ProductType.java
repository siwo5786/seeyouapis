package com.seeyou.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProductType {

    RAW_MATERIAL("원자재"),

    FINISHED_PRODUCT("완제품");

    private final String name;
}
