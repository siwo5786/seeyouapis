package com.seeyou.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MoneyType {

    SALARY("시급"),

    WAGE("연봉");

    private final String name;
}
