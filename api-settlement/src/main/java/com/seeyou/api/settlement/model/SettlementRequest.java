package com.seeyou.api.settlement.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class SettlementRequest {
    @ApiModelProperty(notes = "정산년도", required = true)
    @NotNull
    private String settlementYear;

    @ApiModelProperty(notes = "정산월", required = true)
    @NotNull
    private String settlementMonth;
}
