package com.seeyou.api.settlement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSettlementApplication {
    public static void main(String[] args) {
        SpringApplication.run(ApiSettlementApplication.class, args);
    }
}