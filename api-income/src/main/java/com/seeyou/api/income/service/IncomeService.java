package com.seeyou.api.income.service;

import com.seeyou.api.income.entity.Income;
import com.seeyou.api.income.model.IncomeItem;
import com.seeyou.api.income.model.IncomeRequest;
import com.seeyou.api.income.model.IncomeResponse;
import com.seeyou.api.income.model.IncomeSearchRequest;
import com.seeyou.api.income.repository.IncomeRepository;
import com.seeyou.common.exception.CMissingDataException;
import com.seeyou.common.response.model.ListResult;
import com.seeyou.common.response.service.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class IncomeService {
    private final IncomeRepository incomeRepository;
    private final EntityManager entityManager;

    public void setIncome(IncomeRequest request) {
        Income income = new Income.Builder(request).build();
        incomeRepository.save(income);
    }

    public ListResult<IncomeItem> getList(int page, IncomeSearchRequest request) {
        PageRequest pageRequest = ListConvertService.getPageable(page, 10);
        Page<Income> originList = getList(pageRequest, request);

        List<IncomeItem> result = new LinkedList<>();


        originList.forEach(e -> result.add(new IncomeItem.Builder(e).build()));

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    private Page<Income> getList(Pageable pageable, IncomeSearchRequest request) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Income> criteriaQuery = criteriaBuilder.createQuery(Income.class);

        Root<Income> root = criteriaQuery.from(Income.class);

        List<Predicate> predicates = new LinkedList<>();
        if (request.getIncomeYear() != null) predicates.add(criteriaBuilder.equal(root.get("incomeYear"), request.getIncomeYear()));
        if (request.getIncomeMonth() != null) predicates.add(criteriaBuilder.equal(root.get("incomeMonth"), request.getIncomeMonth()));
        if (request.getIncomeCategory() != null) predicates.add(criteriaBuilder.like(root.get("incomeCategory"), "%" + request.getIncomeCategory() + "%"));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);
        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("id")));

        TypedQuery<Income> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public IncomeResponse getIncome(long id) {
        Income income = incomeRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new IncomeResponse.Builder(income).build();
    }

    public void putIncome(long id, IncomeRequest request) {
        Income income = incomeRepository.findById(id).orElseThrow(CMissingDataException::new);
        income.putIncome(request);
        incomeRepository.save(income);
    }
}
