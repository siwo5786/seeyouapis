package com.seeyou.api.income.entity;

import com.seeyou.api.income.model.IncomeRequest;
import com.seeyou.common.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String incomeYear;

    @Column(nullable = false)
    private String incomeMonth;

    @Column(nullable = false, length = 20)
    private String incomeCategory;

    @Column(nullable = false)
    private Double price;

    public void putIncome(IncomeRequest request) {
        this.incomeYear = request.getIncomeYear();
        this.incomeMonth = request.getIncomeMonth();
        this.incomeCategory = request.getIncomeCategory();
        this.price = request.getPrice();
    }

    private Income(Builder builder) {
        this.incomeYear = builder.incomeYear;
        this.incomeMonth = builder.incomeMonth;
        this.incomeCategory = builder.incomeCategory;
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<Income> {
        private final String incomeYear;
        private final String incomeMonth;
        private final String incomeCategory;
        private final Double price;

        public Builder(IncomeRequest request) {
            this.incomeYear = request.getIncomeYear();
            this.incomeMonth = request.getIncomeMonth();
            this.incomeCategory = request.getIncomeCategory();
            this.price = request.getPrice();
        }

        @Override
        public Income build() {
            return new Income(this);
        }
    }
}
